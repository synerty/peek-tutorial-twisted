from vortex.DeferUtil import (
    deferToThreadWrapWithLogger,
    isMainThread,
    noMainThread,
    yesMainThread,
)
from twisted.internet import reactor
import logging

logger = logging.getLogger(__name__)


@deferToThreadWrapWithLogger(logger)
def notInMainThread():
    # isMainThread returns a bool indicating whether it is in the main thread
    print("notInMainThread running in main thread:", isMainThread())

    # noMainThread does not return a bool, it raises an exception if it is in the main thread
    noMainThread()


def inMainThread():
    # isMainThread returns a bool indicating whether it is in the main thread
    print("inMainThread running in main thread:", isMainThread())

    # noMainThread does not return a bool, it raises an exception if it is in the main thread
    yesMainThread()


if __name__ == "__main__":
    reactor.callLater(0.1, notInMainThread)
    reactor.callLater(0.1, inMainThread)
    reactor.callLater(3.2, reactor.stop)
    reactor.run()
