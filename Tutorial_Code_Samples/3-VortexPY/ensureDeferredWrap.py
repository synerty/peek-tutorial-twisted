from twisted.trial import unittest
from twisted.internet.defer import Deferred
from vortex.DeferUtil import ensureDeferredWrap
import time


class ExampleTest(unittest.TestCase):
    def testVortex(self):
        async def wait1():
            time.sleep(1)
            return True

        @ensureDeferredWrap
        async def usuallyAsync():
            return await wait1()

        d = usuallyAsync()
        self.assertIsInstance(d, Deferred)
