from twisted.trial import unittest
from twisted.internet.defer import Deferred
from vortex.DeferUtil import maybeDeferredWrap


class ExampleTest(unittest.TestCase):
    def testVortex(self):
        @maybeDeferredWrap
        def notUsuallyDeferred():
            return True

        d = notUsuallyDeferred()
        self.assertIsInstance(d, Deferred)
