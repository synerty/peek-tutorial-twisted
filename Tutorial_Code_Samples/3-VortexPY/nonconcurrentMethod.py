from vortex.DeferUtil import nonConcurrentMethod, deferToThreadWrapWithLogger
from twisted.internet import reactor
import time, logging

logger = logging.getLogger(__name__)


@deferToThreadWrapWithLogger(logger)
@nonConcurrentMethod
def usuallyConcurrent():
    time.sleep(2)
    print("Function Called")


if __name__ == "__main__":
    reactor.callLater(0.1, usuallyConcurrent)
    reactor.callLater(0.1, usuallyConcurrent)
    reactor.callLater(3.5, reactor.stop)
    reactor.run()
