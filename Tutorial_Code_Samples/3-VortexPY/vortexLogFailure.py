from twisted.trial import unittest
from twisted.internet import defer
from vortex.DeferUtil import vortexLogFailure
import logging

logger = logging.getLogger(__name__)


class ExampleTest(unittest.TestCase):
    def testVortex(self):
        def alwaysFails():
            d = defer.Deferred()
            d.addErrback(vortexLogFailure, logger, consumeError=True)
            d.errback(1)

        alwaysFails()
