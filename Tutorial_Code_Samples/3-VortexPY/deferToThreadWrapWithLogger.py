from vortex.DeferUtil import deferToThreadWrapWithLogger
from twisted.internet import reactor
import time, logging

logger = logging.getLogger(__name__)


@deferToThreadWrapWithLogger(logger)
def usuallyBlocking():
    time.sleep(2)
    print("Blocker ran outside of main thread")


if __name__ == "__main__":
    reactor.callLater(1, usuallyBlocking)
    reactor.callLater(1.1, print, "Runs in Main Thread")
    reactor.callLater(3.2, reactor.stop)
    reactor.run()
