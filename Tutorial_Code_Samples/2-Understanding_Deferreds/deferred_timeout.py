from twisted.internet import reactor
from twisted.internet.defer import TimeoutError, CancelledError, Deferred
from twisted.trial import unittest


class DeferredExampleTest(unittest.TestCase):
    def testTimeOutIn5(self):
        def timeOutIn5() -> Deferred:
            d = Deferred()
            d.addTimeout(5, reactor)
            d.addErrback(print)
            return d

        # A timed-out Deferred should always raise a TimeoutError
        self.assertRaises(TimeoutError, self.addCleanup(timeOutIn5))
        self.assertRaises(CancelledError, self.addCleanup(timeOutIn5))
