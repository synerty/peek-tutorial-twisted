from twisted.internet.defer import inlineCallbacks
from twisted.internet.task import deferLater
from twisted.internet import reactor
from twisted.trial import unittest


class ExampleTest(unittest.TestCase):
    @inlineCallbacks
    def testWithInlineCallback(self):
        # Create a  Deferred and wait for it with @inlineCallbacks
        d = yield deferLater(reactor, 2, lambda: True)
        print(d)
        print("x")
        return d

    def testWithoutInlineCallback(self):
        # Create a  Deferred and continue
        d = deferLater(reactor, 2, lambda: True)
        d.addCallback(print)
        print(d)
        print("x")
        return d
