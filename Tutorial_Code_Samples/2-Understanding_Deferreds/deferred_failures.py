from twisted.internet.defer import Deferred, fail, succeed, CancelledError
from twisted.trial import unittest


class ExampleTests(unittest.TestCase):
    def testFailureValue(self):

        # Set up a function to examine our Failure
        def assessFailValue(d) -> Deferred:

            # If the first exception is a CancelledError, a user cancelled it
            if type(d.value) == CancelledError:

                # So we don't consider it an error past this point
                return succeed("Eventual Success!")
            else:

                # /continue to handle the error elsewhere/
                return fail(1)

        # create a deferred that we will fail
        d = Deferred()
        d.addErrback(assessFailValue)
        d.addCallback(print)

        # Pretend a user cancelled our Deferred, causing it to errback
        d.cancel()
        return d

    def testFailureCheck(self):

        # Set up a function to examine our Failure
        def assessFailCheck(d) -> Deferred:

            # If the first exception is one we are ok with
            if d.check(TimeoutError, CancelledError, 1) != None:

                # So we don't consider it an error past this point
                return succeed("Eventual Success!")
            else:
                # /continue to handle the error elsewhere/

                return fail(1)

        # create a deferred that we will fail
        d = Deferred()
        d.addErrback(assessFailCheck)
        d.addCallback(print)

        # Pretend a user cancelled our Deferred, causing it to errback
        d.cancel()
        return d
