from twisted.internet import defer
from twisted.trial import unittest


class ExampleTests(unittest.TestCase):
    def testDeferredChaining(self):
        # Create two Deferreds
        deferredGetsChained = defer.Deferred()
        deferredAlsoGetsChained = defer.Deferred()
        deferredMakesChain = defer.Deferred()

        # Give the chain-ee a callback
        deferredGetsChained.addCallback(print, "So was I!")
        deferredAlsoGetsChained.addCallback(print, "Me too!")

        # Chain the first Deferred to the second
        deferredMakesChain.chainDeferred(deferredGetsChained)
        deferredMakesChain.chainDeferred(deferredAlsoGetsChained)

        # Call the first Deferred back to trigger the callback of the other
        deferredMakesChain.callback("I was called back!")
