from twisted.internet import defer
from twisted.trial import unittest


class ExampleTest(unittest.TestCase):
    def testProcessWithSemaphore(self):
        # Setup variables
        maxRun = 2
        taskList = [1, 2, 3, 4, 5, 6]
        deferrals = []

        # Have a Semaphore run functions
        # and create Deferreds in a controlled manner
        sem = defer.DeferredSemaphore(maxRun)
        for task in taskList:
            d = sem.run(print, task)
            deferrals.append(d)

        # (Use a Deferredlist to await all callbacks)
        dList = defer.DeferredList(deferrals)
        dList.addCallback(print)

        return dList
