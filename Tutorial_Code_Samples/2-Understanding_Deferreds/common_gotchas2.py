from twisted.internet import reactor, defer


def returnUncalledDeferred() -> defer.Deferred:
    def getUsername():
        # The callback that is never called
        return "Jimmy"

    # We create a Deferred
    d = defer.Deferred()

    # Add a callback that will get a name
    d.addCallback(getUsername)

    # Accidentally return the Deferred before called its callback
    return d


if __name__ == "__main__":
    # This function will return a Deferred object instead of a username
    username = returnUncalledDeferred()
    print("Hello,", username)
    reactor.run()
