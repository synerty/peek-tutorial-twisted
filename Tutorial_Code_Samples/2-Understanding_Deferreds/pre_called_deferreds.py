from twisted.internet.defer import CancelledError, fail, succeed
from twisted.trial import unittest


class ExampleTests(unittest.TestCase):
    def testPreCalledSuccess(self):
        # Create a deferred that has already resolved
        deferredPreCalledSuccess = succeed("OK")

        # Create a Deferred and Cancel it
        deferredPreCalledFail = fail(1)

        # Returning a pre-cancelled Deferred fails with a CancelledError
        self.assertFailure(deferredPreCalledFail, int)
        self.assertEqual("OK", deferredPreCalledSuccess.result)
