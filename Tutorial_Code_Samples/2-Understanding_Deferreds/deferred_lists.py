from twisted.internet.defer import DeferredList, inlineCallbacks
from twisted.internet.task import deferLater
from twisted.internet import reactor
from twisted.trial import unittest


class ExampleTest(unittest.TestCase):
    def testWithDeferredList(self):
        # Manage multiple Deferreds with a  DeferredList
        d1 = deferLater(reactor, 1, lambda: "Called1")
        d2 = deferLater(reactor, 2, lambda: "Called2")
        dList = DeferredList([d1, d2])
        dList.addCallback(print)
        return dList

    @inlineCallbacks
    def testWithInlineDeferredList(self):
        # Manage multiple Deferreds with a  DeferredList
        d1 = deferLater(reactor, 1, lambda: "Called1")
        d2 = deferLater(reactor, 2, lambda: "Called2")
        dList = DeferredList([d1, d2])
        dList.addCallback(print)
        yield dList
        return dList.resultList

    @inlineCallbacks
    def testWithoutDeferredList(self):
        # Manage multiple Deferreds without a  DeferredList
        d1 = deferLater(reactor, 1, lambda: "Called1")
        d2 = deferLater(reactor, 2, lambda: "Called2")
        d1.addCallback(print)
        d2.addCallback(print)
        yield d1
        yield d2
        return d1, d2
