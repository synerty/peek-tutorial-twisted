from twisted.internet import defer
from twisted.trial import unittest


class ExampleTests(unittest.TestCase):
    def testCallbackPropagationExample(self):
        def cbA(data):
            d = defer.Deferred()
            d.addCallback(cbB)
            d.callback(True)
            return d

        def cbB(data):
            d = defer.Deferred()
            d.addErrback(ebC)
            d.errback(1)
            return d

        def ebC(data):
            raise Exception("NO")

        d = defer.Deferred()
        d.addCallback(cbA)
        d.callback("OK")
        return d

        # Demonstrate that callbacks and errbacks are created in pairs

    def testCallbackErrbackPairs(self):
        def cbA(data):
            d = defer.Deferred()
            d.addErrback(ebB)
            # do something that will call d.errback(x)
            return d

        def ebB(data):
            d = defer.Deferred()
            d.addCallbacks(cbC, ebC)
            # do things that will call either:
            # d.callback(x) or d.errback(x)
            return d

        def cbC(data):
            return True

        def ebC(data):
            raise Exception("NO")

        d = defer.Deferred()
        d.addCallback(cbA)
        # do something that will eventually call d.callback(x)
        return d

    # Demonstrate that an individual Deferred can have multiple callbacks
    def testMultipleCallbacks(self):
        def returnA(d):
            print(d)
            return "A"

        def returnB(d):
            print(d)
            return "B"

        d = defer.Deferred()
        d.addCallback(returnA)
        d.addCallback(returnB)
        d.callback("Start")
        print(d)
        return d

    # Demonstrate an Errback continuing as a callback
    def testErrbackContinues(self):
        def ebA(data):
            d = defer.Deferred()
            d.addCallback(cbB)
            d.callback("Can you handle it?")
            return d

        def cbB(data):
            print(data)
            return "The Failure was handled!"

        d = defer.Deferred()
        d.addErrback(ebA)
        d.addCallback(print)
        d.errback(1)
        return d
