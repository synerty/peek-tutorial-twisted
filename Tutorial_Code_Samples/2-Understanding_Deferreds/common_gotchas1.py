from twisted.internet import reactor, defer


def yieldWithoutInlineCallbacks():
    # We create a Deferred
    d = defer.Deferred()

    # Pretend to enter a name
    d.callback("Jimmy")

    # Accidentally call yield without @inlineCallbacks
    yield d
    return d


if __name__ == "__main__":
    # This function will return a generator object instead of a yielded Deferred, and we never noticed.
    username = yieldWithoutInlineCallbacks()

    # Some time later...
    print("Hello,", username)
    reactor.run()
