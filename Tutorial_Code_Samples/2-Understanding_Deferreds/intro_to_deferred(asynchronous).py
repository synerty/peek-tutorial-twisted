from twisted.internet import reactor, defer


def print_me(value):
    print(value)


def waitTime(name, time) -> defer.Deferred:
    value = defer.Deferred()
    message = " took {} seconds".format(time)
    print(name, time)
    reactor.callLater(time, value.callback, name + message)
    return value


print("Starting: ")

# Define two variables that will take a while to resolve
slow = waitTime("'slow'", 1)
fast = waitTime("'fast'", 5)

# Show that they are being Deferred
print("slow is: ", slow)
print("fast is: ", fast)

# Give them callbacks
slow.addCallback(print_me)
fast.addCallback(print_me)

reactor.run()
