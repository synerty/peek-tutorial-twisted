from twisted.internet import reactor, defer


def print_me(value):
    print(value)
    return True


def wait_five(name) -> str:
    value = defer.Deferred()
    reactor.callLater(5, value.callback, name + " took 5 seconds")
    return value


def wait_ten(name) -> str:
    value = defer.Deferred()
    reactor.callLater(10, value.callback, name + " took 10 seconds")
    return value


print("Starting: ")

# Define two variables that will take a while to resolve
slow = wait_ten("'slow'")
fast = wait_five("'fast'")

# Show that they are being Deferred
print("slow is: ", slow)
print("fast is: ", fast)

# Give them callbacks
slow.addCallback(print_me)
fast.addCallback(print_me)

reactor.run()
