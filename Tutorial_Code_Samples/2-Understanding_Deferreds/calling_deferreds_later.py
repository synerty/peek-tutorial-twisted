from twisted.internet import reactor, task, defer
from twisted.trial import unittest


class ExampleTests(unittest.TestCase):
    def testCallLater(self):
        # Creates a Deferred and has the reactor call it back
        d = defer.Deferred()
        reactor.callLater(1, d.callback, "OK")
        return d

    def testDeferLater(self):
        # Creates a Deferred and has the reactor call it back
        d = task.deferLater(reactor, 1, print, "OK")
        assert isinstance(d, defer.Deferred), "deferLater returns a Deferred"
        return d

    def testLoopingCall(self):
        # Allows a function to be called repeatedly
        c = task.LoopingCall(print, "I'll be back")
        d = c.start(1)
        reactor.callLater(3, c.stop)
        return d
