from twisted.internet.defer import CancelledError, Deferred
from twisted.trial import unittest


class ExampleTests(unittest.TestCase):
    def testCancelsImmediately(self):
        # Create a Deferred and Cancel it
        def cancelImmediately() -> Deferred:
            d = Deferred()
            d.cancel()
            return d

        # Returning a pre-cancelled Deferred fails with a CancelledError
        self.assertFailure(cancelImmediately(), CancelledError)

    def testCustomCanceller(self):
        # The Custom Canceller function
        def printWhenCancelled(d: Deferred) -> Deferred:
            print("I was Cancelled")
            return d

        # Add a custom canceller and immediately call it
        def createWithCusomCanceller() -> Deferred:
            d = Deferred(printWhenCancelled)
            d.cancel()
            return d

        self.assertFailure(createWithCusomCanceller(), CancelledError)
