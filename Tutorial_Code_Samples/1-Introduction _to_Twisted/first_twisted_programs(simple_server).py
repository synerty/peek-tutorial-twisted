from twisted.internet import reactor
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import ServerFactory
from twisted.internet.endpoints import TCP4ServerEndpoint


class ServerProtocol(Protocol):
    def connectionMade(self):
        print("New Connection")
        self.transport.write(b"Hi from server")
        self.transport.loseConnection()


class TutorialServerFactory(ServerFactory):
    def buildProtocol(self, addr):
        print(addr)
        return ServerProtocol()


if __name__ == "__main__":
    endpoint = TCP4ServerEndpoint(reactor, 2000)
    endpoint.listen(TutorialServerFactory())
    reactor.run()
