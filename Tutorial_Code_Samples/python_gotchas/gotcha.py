import unittest


class ExampleTests(unittest.TestCase):
    def testGotcha(self):
        nums = [1, 2, 3, 4, 3, 2, 1]
        print("Before:", nums)

        for ind, n in enumerate(nums):
            if n < 3:
                del nums[ind]

        print("After:", nums)
        print("Expected: nums [3, 4, 3]")
